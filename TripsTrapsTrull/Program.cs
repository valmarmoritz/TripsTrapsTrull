﻿using System;

namespace TripsTrapsTrull
{
    public enum Staatus { Tühi, X, O };

	class MainClass
    {

		public static void Main(string[] args)
        {
            Console.WriteLine("Trips-Traps-Trull\n");

			bool jätkame;
			do
            {
                Mang mang = new Mang();

                while (!Kontrolli.K6ikVäljadTäis(mang) && (Kontrolli.KasOnV6itnud(mang) == Staatus.Tühi))
                {
                    mang.Joonista();
                    Käik.Käi(mang);
                }
                mang.Joonista();
                if (Kontrolli.K6ikVäljadTäis(mang)) Console.WriteLine("See on viik");
                else switch (Kontrolli.KasOnV6itnud(mang))
                    {
                        case Staatus.X:
                            Console.WriteLine("X-id võitsid!");
                            break;
                        case Staatus.O:
                            Console.WriteLine("O-d võitsid!");
                            break;
                    }

                Console.Write("Kas jätkame (J/E)? ");
                string sisend = Console.ReadLine();
                if (sisend != "")
                    switch (sisend.Trim().ToUpper()[0])
                    {
                        case 'J':
                        case 'Y':
                            jätkame = true;
                            break;
                        case 'E':
                        case 'N':
                            jätkame = false;
                            break;
                        default:
                            jätkame = false;
                            break;
                    }
                else jätkame = true;
            }
            while (jätkame) ;
        }
    }
}
