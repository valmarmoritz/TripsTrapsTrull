﻿using System;
namespace TripsTrapsTrull
{
    public class Mang
    {
        public Staatus[,] staatus;

        public Mang()
        {
            staatus = new Staatus[3, 3];
			for (int r = 0; r < 3; r++)
			{
				for (int v = 0; v < 3; v++)
				{
                    staatus[r, v] = Staatus.Tühi;
				}
			}
        }

        public bool Käi(int r, int v, Staatus staatus)
        {
            if (this.staatus[r, v] == Staatus.Tühi)
            {
                this.staatus[r, v] = staatus;
                return true;
            }
            else return false;
        }

        public void Joonista()
        {
            for (int r = 0; r < 3; r++)
            {
                for (int v = 0; v < 3; v++)
                {
                    char symbol;
                    switch (staatus[r, v])
                    {
                        case Staatus.Tühi:
                            symbol = ' ';
                            break;
                        case Staatus.X:
                            symbol = 'X';
                            break;
                        case Staatus.O:
                            symbol = 'O';
                            break;
                        default:
                            symbol = '-';
                            break;
                    }
                    Console.Write($" {symbol}");
                    if (v < 2) Console.Write(" |");
                }
                Console.WriteLine();
                if (r<2) Console.WriteLine("---+---+---");
            }
        }
    }
}
