﻿using System;
namespace TripsTrapsTrull
{
    public class Käik
    {
        static Staatus käiguKord = new Staatus();

        static Käik()
        {
            VahetaKäiguKord();
        }

        public static void Käi(Mang mang)
        {
            Console.Write($"{käiguKord} käik (rida veerg): ");

            string sisend = Console.ReadLine();

			try
			{
                string[] koht = sisend.Split();
                if (koht.Length != 2) throw new ArgumentException();

				if (!mang.Käi(Convert.ToInt32(koht[0]) - 1, Convert.ToInt32(koht[1]) - 1, käiguKord))
                {
                    Console.WriteLine("Sinna ei saa käia. Proovi uuesti!");
                }
                else
                {
                    VahetaKäiguKord();
                }
            }
            catch
            {
                Console.WriteLine($"'{sisend}' - sry, ma ei saanud sellest aru.\nAnna oma käik kujul 'rea number tühik veeru number'.");
            }
        }
        internal static void VahetaKäiguKord()
        {
			if (käiguKord == Staatus.X) käiguKord = Staatus.O;
			else käiguKord = Staatus.X;
		}
    }
}
