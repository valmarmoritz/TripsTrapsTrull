﻿using System;
namespace TripsTrapsTrull
{
    public class Kontrolli
    {
        public static Staatus KasOnV6itnud(Mang mang)
        {
            if (KontrolliV6itu(mang, Staatus.X)) return Staatus.X;
            if (KontrolliV6itu(mang, Staatus.O)) return Staatus.O;
            return Staatus.Tühi;
        }

        private static bool KontrolliV6itu(Mang mang, Staatus staatus)
        {
            // kontrollime kõik read
            for (int r = 0; r < 3; r++)
                if (KontrolliRida(mang, r, staatus)) return true;

            // kontrollime kõik veerud
            for (int v = 0; v < 3; v++)
                if (KontrolliVeerg(mang, v, staatus)) return true;

            // kontrollime diagonaalid
            if (KontrolliDiagonaalAlla(mang, staatus)) return true;
            if (KontrolliDiagonaalYles(mang, staatus)) return true;

            return false;
        }

        private static bool KontrolliRida(Mang mang, int r, Staatus staatus)
        {
            for (int v = 0; v < 3; v++)
                if (mang.staatus[r, v] != staatus) return false;
            return true;
        }
        private static bool KontrolliVeerg(Mang mang, int v, Staatus staatus)
        {
            for (int r = 0; r < 3; r++)
                if (mang.staatus[r, v] != staatus) return false;
            return true;
        }
        private static bool KontrolliDiagonaalAlla(Mang mang, Staatus staatus)
        {
            for (int i = 0; i < 3; i++)
                if (mang.staatus[i, i] != staatus) return false;
            return true;
        }
        private static bool KontrolliDiagonaalYles(Mang mang, Staatus staatus)
        {
            for (int i = 0; i < 3; i++)
                if (mang.staatus[i, 2 - i] != staatus) return false;
            return true;
        }

        public static bool K6ikVäljadTäis(Mang mang)
        {
            for (int r = 0; r < 3; r++)
                for (int v = 0; v < 3; v++)
                    if (mang.staatus[r, v] == Staatus.Tühi) return false;
            return true;
        }
    }
}